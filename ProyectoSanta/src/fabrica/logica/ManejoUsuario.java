/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fabrica.logica;


import agenda.datos.Ninos;
import agenda.datos.Pedidos;
import agenda.datos.Usuario;
import archivodao.ArchivoDao;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import fabrica.util.Util;

/**
 *
 * @author dell
 */
public class ManejoUsuario {
    private Ninos[] ninos;
    
    private Pedidos[] Pedido;
    ArchivoDao ma  = new ArchivoDao();
    private static final String RUTA_USER = "datos/Ussuario.txt";
    private static final String RUTA_NINO = "datos/Ninos.txt";

    public ManejoUsuario() {
        ninos = new Ninos[25];
    }
    
    
    /**
     * Escribe el usuario nuevo en la ruta del archivo
     * @param nueva Usuario nuevo 
     */
    public void guardarUsuario(Usuario nueva){
        ma.escribir(RUTA_USER, nueva.getInfo());

    }
    /**
     * lee usuarios en archivo
     * @param rutaAchivo
     * @return 
     */
    public Usuario[] leerUsuarios(String rutaAchivo){
        String[] usuarios = ma.leer(RUTA_USER).split("\n");
        Usuario[] arr = new Usuario[usuarios.length];
        for (int i = 0; i < usuarios.length; i++) {
            String[] datoUsuario = usuarios[i].split(",");
            Usuario usuario = new Usuario();
            usuario.setCedulaID(datoUsuario[0]);
            usuario.setNombre(datoUsuario[1]);
            usuario.setContrasena(datoUsuario[2]);
            usuario.setFechaNacimiento(Util.convertirFecha(datoUsuario[3]));
            usuario.setTipoUsuario(Boolean.parseBoolean(datoUsuario[4]));
            usuario.setCorreo(datoUsuario[5]);
            arr[i] = usuario;
      
       
    }
        return arr;
    }
    /**
     * Recorre la lista de Usuarios para saber si ese usuario existe y si no los
     * escribe en el archivo
     * @param listaUsuarios Lista de Usuario nueva
     */
    public void guardarUsuario(Usuario[] listaUsuarios ){
        
        for (int i = 0; listaUsuarios.length < 10; i++) {
            guardarUsuario(listaUsuarios);
        }
    }
    /**
     * Lee la ruta de Usuarios la recorre para verificar si los datos contraseña
     * y nombre de usuario son validos y existen.
     * @param usernameScreen Nombre de Usuario que digita el Usuario en pantalla.
     * @param passwordScreen Contraseña que digita el Usuario en pantalla.
     * @return devulve true, si la contraseña y nombre de usuario existen en 
     * el archivo de resgistro de usuario y false en caso contrario.
     */
    public boolean verificarAcceso(String usernameScreen, String passwordScreen){
        Usuario[] listaUsuarios = leerUsuarios(RUTA_USER);
        Usuario verificar = new Usuario();
        for (int i = 0; i < listaUsuarios.length; i++) {
            verificar = listaUsuarios[i];
            String password = verificar.getContrasena();
            String userName = verificar.getCedulaID();
            if(userName.equals(usernameScreen)&& (password.equals(passwordScreen))){
               return true;
            }
        }
        return false;
    }
    /**
     * Lee la ruta de Usuarios la recorre para verificar si los datos contraseña
     * y nombre de usuario son validos y existen.
     * @param usernameScreen Nombre de Usuario que digita el Usuario en pantalla.
     * @param passwordScreen Contraseña que digita el Usuario en pantalla.
     * @return devulve true, si la contraseña y nombre de usuario existen en 
     * el archivo de resgistro de usuario y false en caso contrario.
     */
    public boolean verificarCedula(String usernameScreen){
        Usuario[] listaUsuarios = leerUsuarios(RUTA_USER);
        Usuario verificar = new Usuario();
        for (int i = 0; i < listaUsuarios.length; i++) {
            verificar = listaUsuarios[i];
            String userName = verificar.getCedulaID();
            if(userName.equals(usernameScreen)){
               return true;
            }
        }
        return false;
    }
    
    /**
     * Lee la ruta de Usuarios la recorre para verificar si los datos contraseña
     * y nombre de usuario son validos y existen.
     * @return devulve true, si la contraseña y nombre de usuario existen en 
     * el archivo de resgistro de usuario y false en caso contrario.
     */
    public boolean verificarTipoUsuario(String ced){
        Usuario[] listaUsuarios = leerUsuarios(RUTA_USER);
        Usuario verificar = new Usuario();
        for (int i = 0; i < listaUsuarios.length; i++) {
            verificar = listaUsuarios[i];
            String userName = verificar.getCedulaID();
            if(userName.equals(ced)){
                boolean sacarTipoUser = verificar.getTipoUsuario();
               return sacarTipoUser;
            }
        }
        return false;
    }
    
    public String impUsuarios(Usuario[] usuarios) {
        String msj = "";
        String formato = "%d. %s : %s, %s,%s,%s,%s\n";

        for (int i = 0; i < usuarios.length; i++) {
            Usuario m = usuarios[i];
            if (m == null) {
                return msj;
            }
            
            
            msj += String.format(formato, (i + 1),
                    m.getNombre(), m.getCorreo(),m.getCedulaID(), m.getContrasena(),m.getCorreo(),m.getNombre());
        }
        return msj;
    }
    /**
     * Busca El Usuario por numero de cedula 
     * @param ced
     * @return Lista de Usuarios
     */
    public Usuario buscarUsuario(String ced){
        Usuario[] listaUsuarios = leerUsuarios(RUTA_USER);
        Usuario verificar = new Usuario();
        for (int i = 0; i < listaUsuarios.length; i++) {
            verificar = listaUsuarios[i];
            String cedula = verificar.getCedulaID();
            if(cedula.equals(ced)){
                String msj = "";
                String formato = " %d. %s : %s %s %s %s %s %s\n";
                    Usuario user = listaUsuarios[i];
                   
                return user;
            }
        }
        return null;
    }
    /**
     * Imprime todos los usuarios por numero de cedula
     * @param ced
     * @return Usuario
     */
    public String impUsuario(String ced){
        Usuario[] listaUsuarios = leerUsuarios(RUTA_USER);
        Usuario verificar = new Usuario();
        for (int i = 0; i < listaUsuarios.length; i++) {
            verificar = listaUsuarios[i];
            String cedula = verificar.getCedulaID();
            if(cedula.equals(ced)){
                String msj = "";
                String formato = " %d. %s : %s %s %s %s %s \n";
                    Usuario user = listaUsuarios[i];
                    
                    
                    msj = String.format(formato,(i + 1),
                    user.getNombre(), "\nCorreo: "+ user.getCorreo()+"\n","Cedula: "+ user.getCedulaID()+"\n"
                            +" años" +"\n","Contraseña: "+ user.getContrasena()+"\n","Usuario: "+ user.getNombre()+"\n","Telefono: "+ user.getCorreo()+"\n" );
                
                return msj;
            }
        }
        return null;
    
    }
    /**
     * Edita la informacion del usuario
     * @param info
     * @param temp 
     */
    public void editarUsuario(String info, Usuario temp) {
        String datos = ma.leer(RUTA_USER);
        datos = datos.replaceAll(info, temp.getInfo());
        ma.escribir(RUTA_USER, datos, false);
    }
    /**
     * Permite a traves de un numero retornar true o false para asignar el 
     * valor booleano al atributo tipo de usuario
     * @param num numero de seleccion 
     * @return 
     */
    public boolean tipoUsuario(int num ){
        if(num==1){
            return true;
        }else if (num==2){
            return false;
        }
        return false;
    }
     /**
     * verifico si el arreglo de niños tiene datos
     *
     * @param nino clase ninos
     * @return si hay datos en el arreglo retorna true si no false
     */
    public boolean ValidarNinos(Ninos nino) {

        for (Ninos nino1 : ninos) {
            if (nino1 != null) {
                return true;
            }
        }
        return false;
    }
    /**
     * valido si la ID digitada por el usuario existe en el arreglo ninos y si
     * es asi valido que no tenga pedidos
     *
     * @param ID pido la ID del niño
     * @param nino clase de niños
     * @param pedidos clase de pedidos
     * @return si ID no tiene pedidos y si existe retorno true de lo contrario
     * false
     */
    public boolean VerificarNinos(String ID, Ninos nino, Pedidos pedidos) {

        if ("X".equals(ID)) {
            return true;
        }
        String nombre;
        boolean validar = false;
        for (Pedidos pedido1 : Pedido) {
            if (pedido1 != null) {
                if (!pedido1.getID().equals(ID)) {
                } else {
                    validar = true;
                }
            }
        }
        if (!validar) {
            for (Ninos nino1 : ninos) {

                if (nino1 != null) {
                    if (nino1.getCedulaID().equals(ID)) {
                        pedidos.setID(ID);
                        nombre = nino1.getNombre();
                        pedidos.setNombre(nombre);
                        return true;
                    }
                }
            }
        }

        return false;
    }
     /**
     * Escribe el usuario nuevo en la ruta del archivo
     * @param nueva Usuario nuevo 
     */
    public void guardarNino(Ninos nueva){
        ma.escribir(RUTA_NINO, nueva.getInfo());

    }
    /**
     * lee lo niños en el archivo
     * @return 
     */
    public Ninos[] leerNino(){
        String[] Ninos = ma.leer(RUTA_NINO).split("\n");
        Ninos[] arr = new Ninos[ninos.length];
        for (int i = 0; i < Ninos.length; i++) {
            String[] datoNinos = Ninos[i].split(",");
            Ninos ninos = new Ninos();
            ninos.setCedulaID(datoNinos[0]);
            ninos.setNombre(datoNinos[1]);
            ninos.setEdad(datoNinos[2]);
            ninos.setDireccion((datoNinos[3]));
            arr[i] = ninos;
      
       
    }
        return arr;
    }
    /**
     * Recorre la lista de Usuarios para saber si ese usuario existe y si no los
     * escribe en el archivo
     * @param listaNinos
     */
    public void guardarNino(Ninos [] listaNinos ){
       
        for (int i = 0; listaNinos.length < 10; i++) {
            guardarNino(listaNinos);
        }
    }
   
    /**
     *aqui registro los niños si la posicion no tiene valores
     * @param nino clase niños
     */
    public void registrarNinos(Ninos nino){
        for (int i = 0; i < ninos.length; i++){
            if (ninos[i] == null){
                ninos[i] = nino;
               
                break;
            }
        }
    }
    public Ninos[] Nino(){
        String[] Ninos = ma.leer(RUTA_NINO).split("\n");
        Ninos[] arr = new Ninos[ninos.length];
        for (int i = 0; i < Ninos.length; i++) {
            String[] datoNinos = Ninos[i].split(",");
            Ninos ninos = new Ninos();
            ninos.setCedulaID(datoNinos[0]);
            ninos.setNombre(datoNinos[1]);
            ninos.setEdad(datoNinos[2]);
            ninos.setDireccion((datoNinos[3]));
            arr[i] = ninos;
      
       
    }
        return arr;
        
    }
public boolean valores(Ninos[] nino){
     for (int i = 0; i < nino.length; i++) {
         if (nino[i] != null) {
             return true;
         }
     }
        return false;
    
}
/**
     * Lee la ruta de Usuarios la recorre para verificar si los datos contraseña
     * y nombre de usuario son validos y existen.
     * @param usernameScreen Nombre de Usuario que digita el Usuario en pantalla.
     * @param passwordScreen Contraseña que digita el Usuario en pantalla.
     * @return devulve true, si la contraseña y nombre de usuario existen en 
     * el archivo de resgistro de usuario y false en caso contrario.
     */
    public boolean verificarCedula_nino(String usernameScreen,Pedidos pedidos){
        Ninos[] listaNinos = leerNino();
        Ninos verificar = new Ninos();
        for (int i = 0; i < listaNinos.length; i++) {
            verificar = listaNinos[i];
            String userID = verificar.getCedulaID();
            String userName = verificar.getNombre();
            if(userID.equals(usernameScreen)){
                pedidos.setID(userID);
                pedidos.setNombre(userName);
                
               return true;
            }else{
                
            }
        }
        return false;
    }
    public int carta(){
        return 0;
    
    }
}