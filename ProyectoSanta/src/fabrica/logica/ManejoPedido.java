/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fabrica.logica;

import agenda.datos.Juguete;
import agenda.datos.Ninos;
import agenda.datos.Pedidos;
import fabrica.util.Util;

/**
 *
 * @author dell
 */
public class ManejoPedido {
    private Pedidos[] Pedido;
    private Juguete[] juguetes;
    private int clavos;
    private int Pintura;
    private int Madera;
    private Iterable<Ninos> ninos;
    private ManejoUsuario logUser = new ManejoUsuario();

    
    
    public ManejoPedido() {
        
        Madera = 10;
        Pintura = 10;
        clavos = 10;
        
        juguetes  = new Juguete[25];
        juguetes[0] = new Juguete("1", "Bicicleta", 2, 2, 3);
        juguetes[1] = new Juguete("2", "Carro", 4, 2, 3);
        juguetes[2] = new Juguete("3", "Barbie", 5, 1, 3);
        juguetes[3] = new Juguete("4", "Avion", 11, 2, 15);
        
        Pedido = new Pedidos[25];
//         Pedido[0] = new Pedidos("Carlitos", "12345678", "Avion" , false);
    }
    
    
    
    /**
     * verifico si el arreglo de pedidos no este vacio
     *
     * @param pedidos clase pedidos
     * @return si hay datos en el arreglo retorna true de lo contrario false
     */
    public boolean ValidarPedidos(Pedidos pedidos) {
        
        for (Pedidos pedido1 : Pedido) {
            if (pedido1 != null) {
                return true;
            }
        }
        return false;
    }
    
    
    
     /**
     * verifico si el arreglo de pedidos no este vacio
     *
     * @param pedidos clase pedidos
     * @return si hay datos en el arreglo retorna true de lo contrario false
     */
    public boolean ValidarPedidosFinalizados(Pedidos pedidos) {
        
        for (Pedidos pedido1 : Pedido) {
            if (pedido1 != null) {
                if (!pedido1.isFinalizado()) {
                   return true; 
                }
            }
        }
        return false;
    }
    /**
     *
     * @param pedidos recorro el arreglo de pedidos y imprime sus valores
     * @param juguete recorro el arreglo de juguetes y valido si el nombre del
     * regalo es igual a el nombre del pedido y imprimo los insumos necesarios
     * para la creacion
     * @return retorna nulo si no hay pedidos
     */
    public String VerPedidos(Pedidos pedidos, Juguete juguete) {

        for (Pedidos pedidos1 : Pedido) {
            if (pedidos1 != null) {
                Util.mostrar("Nombre: " + pedidos1.getNombre()+ "\n"
                + "Cedula de Identidad: " + pedidos1.getID()+ "\n"
                +"Regalo: " + pedidos1.getRegalo()+ "\n"
                +"Finalizado: " + pedidos1.isFinalizado() + "\n");
                for (Juguete juguete1 : juguetes) {
                    if (juguete1 != null) {
                        if (juguete1.getNombre().equals(pedidos1.getRegalo())) {
                            Util.mostrar("Insumos necesarios para la creacion del juguete"+ "\n"
                            +"Necesita un total de metal y madera de: " + juguete1.getMadera()+ "\n"
                            +"Necesita un total de pintura de: " + juguete1.getPintura()+ "\n"
                            +"Necesita un total de clavos y tornillos de:" + juguete1.getClavos() + "\n");
                        }
                    }
                }
            }
        }

        return null;

    }
     /**
     * Recorro los arreglos y suma la cantidad de insumos necesario para crear
     * los pedido y convalido si le alcanzan
     *
     * @param ID La ID del nino
     * @param Juguetes el juguete que el usuario desea pedir
     * @param juguete Arreglo juguetes
     * @param pedidos Arreglo pedidos
     * @return retorna true si el pedido se puede hacer si no false
     */
    public boolean PedidosJuguetes(String ID, String Juguetes, Juguete juguete, Pedidos pedidos) {
        int TotalC = 0;
        int TotalM = 0;
        int TotalP = 0;
        if (Juguetes.equals("X")) {
            return true;
        }

        for (Juguete juguete1 : juguetes) {
            if (juguete1 != null) {
                if (juguete1.getCodigo().equals(Juguetes)) {
                    TotalP += juguete1.getPintura();
                    TotalM += juguete1.getMadera();
                    TotalC += juguete1.getClavos();
                }
                for (Pedidos pedidos1 : Pedido) {
                    if (pedidos1 != null) {
                        if (pedidos1.getRegalo().equals(juguete1.getNombre())) {
                            if (pedidos1.isFinalizado()) {
                                TotalP += juguete1.getPintura();
                                TotalM += juguete1.getMadera();
                                TotalC += juguete1.getClavos();
                            }
                        }
                    }
                }
            }
        }

        Util.mostrar(Integer.toString(TotalC+TotalM+TotalP));
        
        int clavos = this.clavos - TotalC;
        int pintura = this.Pintura - TotalP;
        int metal = this.Madera - TotalM;
        if (clavos > 0 & pintura > 0 & metal > 0) {
            for (Juguete juguete1 : juguetes) {
                if (juguete1 != null) {
                    if (juguete1.getCodigo().equals(Juguetes)) {
                        pedidos.setRegalo(juguete1.getNombre());
                         Util.mostrar("Pedido hecho");
                        return true;
                    }
                }
            }
        }

        return false;

    }
    /**
     * Objetivo del metodo
     *
     * @param pedidos recorro el arreglo pedidos y si la casilla esta vacia se
     * añade el pedido
     */
    public void RegistrarPedido(Pedidos pedidos) {

        for (int i = 0; i < Pedido.length; i++) {
            if (Pedido[i] == null) {
                Pedido[i] = pedidos;
                break;
            }

        }

    }
    /**
     * recorro pedidos si la ID es igual y el pedido no esta finaizado se
     * cancela
     *
     * @param ID le pido al usuario la ID del niño
     * @param pedidos clase pedidos
     * @return retorna true si se cancela el pedido sino false
     */
    public boolean CancelarPedidos(String ID, Pedidos pedidos) {

        if ("X".equals(ID)) {
            return true;
        }

        for (int i = 0; i < Pedido.length; i++) {
            if (Pedido[i] != null) {
                if (Pedido[i].getID().equals(ID) && !Pedido[i].isFinalizado()) {
                    Pedido[i] = null;
                    return true;
                }
            }
        }

        return false;

    }
     /**
     * recorro el arreglo pedidos y finalizo el pedido
     *
     * @param pedidos clase pedidos
     * @return retorno true si se finaliza el pedido
     */
    public boolean validarFinalizados(Pedidos pedidos) {

        for (Pedidos pedidos1 : Pedido) {
            if (pedidos1 != null) {
                if (pedidos1.isFinalizado() == true) {
                    return true;
                }
            }
        }
        return false;
    }
     /**
     *
     * @param pedidos recorro pedidos y si finalizado es igual true
     */
    public void ImprimirColillas(Pedidos pedidos) {
        Ninos[] listaNinos = logUser.leerNino();
        Ninos verificar = new Ninos();
        for (Pedidos pedidos1 : Pedido) {
            if (pedidos1 != null) {
                if (pedidos1.isFinalizado() == true) {
                    for (int i = 0; i < listaNinos.length; i++) {
                            verificar = listaNinos[i];
                            if (verificar != null) {
                                
                            
                            String userID = verificar.getCedulaID();
                            
                            if (userID.equals(pedidos1.getID())) {

                                Util.mostrar("Nombre: " + pedidos1.getNombre()+ "\n"
                                        +"Edad: " + verificar.getEdad() + "\n"
                                        +"Cedula de Identidad: " + pedidos1.getID()+ "\n"
                                        +"Regalo: " + pedidos1.getRegalo()+ "\n"
                                        +"Finalizado: " + pedidos1.isFinalizado()+ "\n"
                                        +"Direccion: " + verificar.getDireccion() + "\n");

                            }
                        }}
                    }
                }
        }
        for (Pedidos pedidos1 : Pedido) {
            if (pedidos1 != null) {
                if (pedidos1.isFinalizado() == true) {
                    int conA = 0;
                    int conC = 0;
                    int conB = 0;
                    int conBi = 0;
                    switch (pedidos1.getRegalo()) {
                        case "Avion":
                            conA++;
                            Util.mostrar("Cantidad de Aviones finalizados: " + conA);
                            break;
                        case "Bicicleta":
                            conBi++;
                            Util.mostrar("Cantidad de bicicletas finalizados: " + conBi);
                            break;
                        case "Carro":
                            conC++;
                            Util.mostrar("Cantidad de carros finalizados: " + conC);
                            break;
                        case "Barbie":
                            conB++;
                            Util.mostrar("Cantidad de barbie finalizados: " + conB);
                            break;
                        default:
                            break;
                    }

                }
            }
        }

    }
     /**
     *
     * @param ID pido la ID del niño al usuario
     * @param pedidos recorro el arreglo verifico que sea igual la ID a la del
     * pedido y tambien si el pedido no esta finalizado
     * @param nino verifico si la ID del niño exista
     * @param usernameScreen
     * @return retorno true si se cumple lo anterior si no false
     */
    public boolean IDNinosFinalizar(Pedidos pedidos,String usernameScreen) {

        if ("X".equals(usernameScreen)) {
            return true;
        }
        boolean validar = false;
        for (Pedidos pedido1 : Pedido) {
            if (pedido1 != null) {
                if (pedido1.getID().equals(usernameScreen)) {
                    if (pedido1.isFinalizado() == false) {
                        validar = true;
                    }
                }
            }
        }

        if (validar) {
        Ninos[] listaNinos = logUser.leerNino();
        Ninos verificar = new Ninos();
        for (int i = 0; i < listaNinos.length; i++) {
            verificar = listaNinos[i];
            String userID = verificar.getCedulaID();
            if(userID.equals(usernameScreen)){
                return true;
            }
            }
            
        }

        return false;

    }
    /**
     *
     * @param ID pido al usuario la id del niño
     * @param pedidos recorro el arreglo y valido la ID si es igual a la ID de
     * algun pedido
     * @param juguete recorro el arreglo valido si el nombre del juguete es
     * igual al nombre del pedido si es asi resto los insumos necesario para
     * crear el pedio vrs los insumos que tiene la fabrica
     * @return si la resta da positiva doy el pedido por finalizado y retorno
     * true de lo contrario retorno false y le imprimo que insumos insuficientes
     * y pedido no se finaliza.
     */
    public boolean RecetasDeJuguetes(String ID, Pedidos pedidos, Juguete juguete) {
        String pedido;
        for (Pedidos pedido1 : Pedido) {
            if (pedido1 != null) {
                if (pedido1.getID().equals(ID)) {
                    pedido = pedido1.getRegalo();
                    for (Juguete juguete1 : juguetes) {
                        if (juguete1 != null) {
                            if (juguete1.getNombre().equals(pedido)) {
                                int clavos = this.clavos - juguete1.getClavos();
                                int pintura = this.Pintura - juguete1.getPintura();
                                int metal = this.Madera - juguete1.getMadera();
                                if (clavos > 0 & pintura > 0 & metal > 0) {
                                    this.clavos = clavos;
                                    this.Pintura = pintura;
                                    this.Madera = metal;

                                    pedido1.setFinalizado(true);
                                    return true;
                                } else {
                                    Util.mostrar("\nInsumos insuficientes");
                                    return false;
                                }

                            }
                        }
                    }

                }
            }
        }
        return false;

        
    }
}
