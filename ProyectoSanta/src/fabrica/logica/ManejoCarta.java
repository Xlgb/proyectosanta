/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fabrica.logica;



import fabrica.util.Util;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author dell
 */
public class ManejoCarta {
/**
     * este lee el archivo en la direccion especificada y almacena la
     * informacion en una variable y la retorna
     *
     * @param Direccion Direccion de la carta
     * @return retorna la carta
     */
    public String LeerCartas(String Direccion) {

        String texto = "";

        try {
            try (BufferedReader bf = new BufferedReader(new FileReader(Direccion))) {
                String bfRead;
                while ((bfRead = bf.readLine()) != null) {//mientras tenga datos
                    texto += bfRead;

                }
            }
        } catch (IOException e) {

            System.err.println("No se ha encontrado el archivo");

        }
        return texto;
    }
    
    /**
     * pido la direccion de la carta retorno la fecha de la carta con el formato
     * dd/MM/yyyy
     *
     * @param fecha la direccion de la carta nada mas que le puse fecha XD
     * @return retorna la fecha de la carta
     */
    public String OBfecha(String fecha) {
        File file = new File(fecha);

        BasicFileAttributes attrs;
        try {
            attrs = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
            FileTime time = attrs.creationTime();

            String pattern = "dd/MM/yyyy";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

            String formatted = simpleDateFormat.format(new Date(time.toMillis()));

            return formatted;
        } catch (IOException e) {
            System.err.println("error" + e);
        }
        return null;
    }
    /**
     * muevo la carta de la direccion actual de la carta hasta el destino que
     * desee
     *
     * @param direccion direccion de la carta
     * @param Carta nombre de la carta
     * @param Destinos carpeta destino
     */
    public void MoverA(String direccion, String Carta, String Destinos) {

        File Destino = new File(Destinos + Carta);
        String C = Destino.getAbsolutePath();
        System.out.println(C);
        Path destino = FileSystems.getDefault().getPath(C);
        Path origen = FileSystems.getDefault().getPath(direccion);

        try {
            Util.mostrar("Se movio correctamente");
            Files.move(origen, destino, StandardCopyOption.REPLACE_EXISTING);

        } catch (IOException e) {
             Util.mostrar("El archivo data no existe.");
            System.err.println(e);

        }

    }
    /**
     * pido la direccion de la carta que desea borrar y se borra
     *
     * @param direccion direccion de la carta
     */
    public void BorrarA(String direccion) {

        try {
            System.out.println(direccion);
            File file = new File(direccion);
            boolean estatus = file.delete();

            if (!estatus) {

                Util.mostrar("Error no se ha podido eliminar el  archivo");

            } else {

                Util.mostrar("Se ha eliminado el archivo exitosamente");

            }

        } catch (Exception e) {

            System.out.println(e);

        }

    }
     /**
     * retorna la fecha del dia actual con formato dd/MM/yyyy
     *
     * @return retorna fecha
     */
    public String FechaDelDia() {

        Date fecha = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return dateFormat.format(fecha);

    }
    
    /**
     *
     * @param Arreglo pido el arreglo de cartas y valido si tienen Cartas
     * @return si es asi retorno true de lo contrario false
     */
    public boolean ValoresEnArreglo(String[] Arreglo) {

        for (String Arreglo1 : Arreglo) {
            if (Arreglo1 != null) {
                return true;
            } else {
            }
        }
        return false;
    }
}