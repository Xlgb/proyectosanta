/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fabrica.logica;

import agenda.datos.Historico;
import agenda.datos.Pedidos;
import fabrica.util.Util;

/**
 *
 * @author dell
 */
public class ManejoConsulta {
    
       private Pedidos[] Pedido;
    private Historico[] Historico;
    private int Clavos;
    private int Pintura;
    private int Madera;
    public ManejoConsulta(){
    Historico = new Historico[25];
    Historico[0] = new Historico("Lunes", 11, 14, 12);
        Historico[1] = new Historico("Martes", 12, 11, 15);
        Historico[2] = new Historico("Miercoles", 17, 14, 18);
        Historico[3] = new Historico("Jueves", 10, 11, 9);
        Historico[4] = new Historico("Viernes", 2, 5, 6);
    
    }
    /**
     * se saca un promedio de acuerdo lo que se gasta por dia segun el historial
     *
     * @param historicos Arreglo que contiene el historial de pedidos de la
     * fabrica
     */
    public void PromedioXDia(Historico historicos) {
        int TotalC;
        int TotalM;
        int TotalP;
        int clavos = 0;
        int pintura = 0;
        int metal = 0;
        int con = 0;
        for (Historico historicos1 : Historico) {
            if (historicos1 != null) {
                clavos += historicos1.getClavos();
                pintura += historicos1.getPintura();
                metal += historicos1.getMadera();
                con++;
            }
        }

        TotalC = clavos / con;
        TotalM = metal / con;
        TotalP = pintura / con;

       Util.mostrar("Promedio de lo que se gasta de clavos y tornillos al Dia: " + TotalC +"\n"+
               "Promedio de lo que se gasta de pintura al Dia: " + TotalP+"\n"+
               "Promedio de lo que se gasta de metal y madera al Dia: " + TotalM);
        
        

    }
     /**
     * imprimo el total de insumos que tiene la fabrica disponibles
     */
    public void ImprimirTotalInsumos() {

        Util.mostrar("\nLe quedan un total de calvos y tornillos de: " + this.Clavos+"\n"
                +"Le quedan un total de pintura de: " + this.Pintura+"\n"
                        +"Le quedan un total de madera y metal de: " + this.Madera + "\n");
        
        

    }
    /**
     * calcula insumos
     * @param historicos 
     */
    public void InsumosSuficientes(Historico historicos) {
        int TotalC;
        int TotalM;
        int TotalP;
        int clavos = 0;
        int pintura = 0;
        int metal = 0;
        int con = 0;
        int DM;
        int DP;
        int DC;
        for (Historico historicos1 : Historico) {
            if (historicos1 != null) {
                clavos += historicos1.getClavos();
                pintura += historicos1.getPintura();
                metal += historicos1.getMadera();
                con++;
            }
        }
//        TotalC / clavos_tornillos
        TotalC = clavos / con;
        TotalM = metal / con;
        TotalP = pintura / con;
        DC = clavos / TotalC;
        DM = Madera / TotalM;
        DP = Pintura / TotalP;
        Util.mostrar("Los insumos que tiene de calvos y tornillos solo le van alcanzar para: " + DC + " Dias"+"\n"
                +"Los insumos que tiene de pintura solo le van alcanzar para: " + DP + " Dias"+"\n"
                +"Los insumos que tiene de madera y matal solo le van alcanzar para: " + DM + " Dias" + "\n");

    }
     /**
     *
     * @param metal la cantidad de metal que desea pedir el usuario
     * @param pintura la cantidad de pintura que desea pedir el usuario
     * @param clavos la cantidad de clavos que desea pedir el usuario
     */
    public void PedirMasInsumos(int metal, int pintura, int clavos) {

        this.Clavos = this.Clavos + clavos;
        this.Madera = this.Madera + metal;
        this.Pintura = this.Pintura + pintura;

    }

}
