/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fabrica.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author allanmual
 */
public class Util {
    

    /**
     * Permite leer un número entero, en caso de error lo solicita nuevamente
     *
     * @param mensaje String que le indica al usuario que debe digitar
     * @return int valor que el usuario digito
     */
    public static int leerInt(String mensaje) {
        String msj = mensaje;
        do {
            try {
                int num = Integer.parseInt(JOptionPane.showInputDialog(msj));
                return num;
            } catch (Exception ex) {
                msj = "Intente nuevamente, " + mensaje;
            }
        } while (true);
    }
    
    /**
     * Permite leer un número con decimales, en caso de error lo solicita
     * nuevamente
     *
     * @param mensaje String que le indica al usuario que debe digitar
     * @param valor double con el valor incial del componente
     * @return double valor que el usuario digito
     */
    public static double leerDouble(String mensaje, double valor) {
        return leerDouble(mensaje, String.valueOf(valor));
    }
    
/**
     * Permite leer un número con decimales, en caso de error lo solicita
     * nuevamente
     *
     * @param mensaje String que le indica al usuario que debe digitar
     * @return double valor que el usuario digito
     */
    public static double leerDouble(String mensaje) {
        return leerDouble(mensaje, "");
    }
    
    /**
     * Permite leer un número con decimales, en caso de error lo solicita
     * nuevamente
     *
     * @param mensaje String que le indica al usuario que debe digitar
     * @param valor Valor incial del componente
     * @return double valor que el usuario digito
     */
    private static double leerDouble(String mensaje, String valor) {
        String msj = mensaje;
        do {
            try {
                double num = Double.parseDouble(JOptionPane.showInputDialog(null, msj, valor));
                return num;
            } catch (Exception ex) {
                msj = "Intente nuevamente, " + mensaje;
            }
        } while (true);
    }
     /**
     * Permite leer un texto requerido, en caso de estar vacio lo solicita
     * nuevamente
     *
     * @param mensaje String que le indica al usuario que debe digitar
     * @return String con el texto digitado
     */
    public static String leerTexto(String mensaje) {
        return leerTexto(mensaje, true, "");
    }

    /**
     * Permite leer un texto requerido, en caso de estar vacio lo solicita
     * nuevamente
     *
     * @param mensaje String que le indica al usuario que debe digitar
     * @param valor String valor incial del cuadro
     * @return String con el texto digitado
     */
    public static String leerTexto(String mensaje, String valor) {
        return leerTexto(mensaje, true, valor);
    }

    /**
     * Permite leer un texto requerido o no, en caso de estar vacio y ser
     * requerido lo solicita nuevamente
     *
     * @param mensaje String que le indica al usuario que debe digitar
     * @param requerido True: para que el usuario tenga que digitar texto
     * obligatoriamente
     * @return String con el texto digitado
     */
    public static String leerTexto(String mensaje, boolean requerido, String valor) {
        String msj = mensaje;
        do {
            String txt = JOptionPane.showInputDialog(null, msj, valor);
            // txt = txt == null ? "" : txt; alternativo

            if (txt != null && (txt.trim().length() > 0 || !requerido)) {
                return txt;
            } else {
                msj = "Datos requeridos, " + mensaje;
            }
        } while (true);
    }
    /**
     * Muestra un mensaje al usuario en un JOP
     *
     * @param mensaje String que se le muestra al usuario
     */
    public static void mostrar(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje);
    }
    /**
     * Muestra un mensaje al usuario en un JOP
     *
     * @param mensaje String que se le muestra al usuario
     */
    public static void mostrarAlerta(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje,"Alerta",JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Muestra un dialogo de corfirmación
     *
     * @param mensaje String que se va a mostrar en el dialogo
     * @return true si el usuario acepta o false en caso contrario
     */
    public static boolean confirmar(String mensaje) {
        int res = JOptionPane.showConfirmDialog(null, mensaje,
                "Veterinaria - UTN v0.1", JOptionPane.YES_NO_OPTION);
        return res == JOptionPane.YES_OPTION;
    }
    /**
     * Solicita en un dialogo una fecha y la retorna, se repite hasta realizarlo
     * correctamente
     *
     * @param mensaje String con el mensaje a solicar
     * @return Date con la fecha digita por el usuario
     */
    public static Date leerFecha(String mensaje) {
        return leerFecha(mensaje, "");
    }

    /**
     * Solicita en un dialogo una fecha y la retorna, se repite hasta realizarlo
     * correctamente
     *
     * @param mensaje String con el mensaje a solicar
     * @return Date con la fecha digita por el usuario
     */
     public static Date leerFecha(String mensaje, String valor) {
        String msj = mensaje;
        do {
            try {
                String dato = JOptionPane.showInputDialog(null, msj, valor);
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                Date fecha = sdf.parse(dato);
                return fecha;
            } catch (ParseException ex) {
                msj = "Intente nuevamente, " + mensaje;
            }
        } while (true);
    }

    public static Date convertirFecha(String fecha) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date res = sdf.parse(fecha);
            return res;
        } catch (ParseException ex) {
            return new Date();
        }
    }
}
