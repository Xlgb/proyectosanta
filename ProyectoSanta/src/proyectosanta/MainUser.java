/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectosanta;

import agenda.datos.Carta;
import agenda.datos.Historico;
import agenda.datos.Juguete;
import agenda.datos.Ninos;
import agenda.datos.Pedidos;
import fabrica.util.Util;
import fabrica.logica.ManejoUsuario;
import agenda.datos.Usuario;
import fabrica.logica.ManejoCarta;
import fabrica.logica.ManejoConsulta;
import fabrica.logica.ManejoPedido;
import java.io.File;
import java.util.Date;

/**
 *
 * @author dell
 */
public class MainUser {
    
     static ManejoUsuario logUser = new ManejoUsuario();
     static Usuario usuario=new Usuario();
     static ManejoCarta logCarta = new ManejoCarta();
     static Pedidos pedido = new Pedidos();
     static ManejoPedido ManejoPedidos = new ManejoPedido();
     static ManejoConsulta ManejoConsutal = new ManejoConsulta();
     static Juguete juguete = new Juguete();
     static Historico historico = new Historico();
     static Ninos nino = new Ninos();
    public static void main(String[] args) {
        
        File ArchivoBuzon = new File("\\Programacion 1\\ProyectoSanta\\proyectosanta\\ProyectoSanta\\Buzon");
        File ArchivoProduccion = new File("\\Programacion 1\\ProyectoSanta\\proyectosanta\\ProyectoSanta\\Produccion");
        File ArchivoPendientes = new File("\\Programacion 1\\ProyectoSanta\\proyectosanta\\ProyectoSanta\\Pendientes");
        final String DireccionBuzon = ArchivoBuzon.getAbsolutePath() + "\\";
        final String DireccionProduccion = ArchivoProduccion.getAbsolutePath() + "\\";
        final String DireccionPendiente = ArchivoPendientes.getAbsolutePath() + "\\";
        String[] Cartas = ArchivoBuzon.list();
        
        String[] CartasPro = ArchivoProduccion.list();
        
        
        
        
        
        String menuAdministrador = ("---El taller de Santa---\n"
                + "1. Registrar Usuario\n"
                + "2. Ingresar al sistema\n"
                + "3. Salir\n"
                + "Digite una opcion:");
        
        String menuTipoUser=("1. Administrador\n"
                + "2.  Empleado");
        
        String menuPreguntaRegis = ("Desea hacer un nuevo registro:\n"
                + "1. Si\n"
                + "2 No\n"
                + "Digite una opción");
        
        String menuSecundario = ("1. Leer cartas\n"
                + "2. Procesar Pedidos\n"
                + "3. Consultas\n"
                + "4. Regresar\n"
                + "Digite una opcion:");
        
        String menuCarta=("---Cartas---\n"
                + "1. Leer cartas del dia\n"
                + "2. Leer cartas por Fecha\n"
                + "3. Regresar\n"
                + "Digite una opción");
        
        String menuPedidos = "---Pedidos---\n                                                                                                                                                \n"
                + "1)Visualizar la pantalla de pedidos\n"
                + "2)Crear pedidos\n"
                + "3)Cancelar pedidos\n"
                + "4)Regresar\n";
        
        String menuJuguetes = "Que juguete desea pedir o X para salir\n"
                + "1)Bicicleta\n"
                + "2)Carro\n"
                + "3)Barbie\n"
                + "4)Avión\n";
        
        String menuConsultas = "\nConsultas                                                                                                                                                 \n"
                + "1)Estado de producción vs insumos\n"
                + "2)Artículos terminados\n"
                + "3)Regresar\n"; 
        
        String menuProdu_Insumo = "Estado de producción vs insumos                                                                                                                                                \n"
                + "1)visualizar cuantos insumos quedan y el promedio\n"
                + "2)pedir más insumos.\n"
                + "3)Regresar\n";
        
        String menuEmpleado = "\nBienvenido ayudante de Santa\n"//Se muestra el menu Santa
                                        + "1)Registrar niños\n"
                                        + "2)Realizar pedidos\n"
                                        + "3)finalizar los pedidos\n"
                                        + "4)Imprimir Colillas\n"
                                        + "5)Salir\n";
       
        
        
        
        
        
        APP:
        while (true) {
            int op = Util.leerInt(menuAdministrador);
            switch (op) {
                case 3:
                    Util.mostrar("Gracias por utilizar la aplicacion");
                    break APP;
                case 1:
                    boolean opcion = false;
                    while (opcion == false) {                        
                        String cedula = Util.leerTexto("Digite su CedulaID:");
                         boolean verificar = logUser.verificarCedula(cedula);
                         if (verificar!=false) {
                        Util.mostrarAlerta("Usuario Ya existe");
                        break;
                    }
                        String contra = Util.leerTexto("Digite su Contraseña:");
                        String nombre = Util.leerTexto("Digite su nombre:");
                        Date fecha = Util.leerFecha("Ingrese su Fecha de Nacimiento (dd/MM/yyyy)");
                        String correo = Util.leerTexto("Digite su correo electronico:");
                        int op2 = Util.leerInt(menuTipoUser);
                        boolean tipoUsuario=logUser.tipoUsuario(op2);
                        Usuario temp = new Usuario ( cedula, nombre, contra,fecha,tipoUsuario,correo);
                        logUser.guardarUsuario(temp);
                        Util.mostrar("El Usuario se Agregó con Exito");
                        int op3 = Util.leerInt(menuPreguntaRegis);
                        if (op3 == 1) {
                            continue;
                        } else {
                            opcion = true;
                        }
                    }
                    break;
                case 2:
                    String cedula = Util.leerTexto("Digite su CedulaID ");
                    String contra = Util.leerTexto("Digite su contraseña");
                    boolean verificar = logUser.verificarAcceso(cedula, contra);
                    if (!verificar) {
                        Util.mostrarAlerta("Contraseña o Usuario Incorrectos\n"
                                + "------Debes registrase antes------");
                        break;
                    }
                    boolean tipoUsuario = logUser.verificarTipoUsuario(cedula);
                    if (tipoUsuario) {
                        Util.mostrar("Usted se ha registrado como Administrador");
                        MENU4:
                        while (true) {                            
                            int op4 = Util.leerInt(menuSecundario); 
                            MENU5:
                            switch (op4) {
                                case 1:
                                    
                                    while (true) {
                                        String[] CartasPen = ArchivoPendientes.list();
                                        int op5 = Util.leerInt(menuCarta);
                                        switch (op5) {
                                            case 1:
                                                if (!logCarta.ValoresEnArreglo(Cartas)) {
                                                    Util.mostrarAlerta("No Hay cartas disponibles");
                                                }
                                                for (String Carta1 : Cartas) {
                                                    
                                                    if (Carta1 != null) {
                                                       if (logCarta.FechaDelDia().equals(logCarta.OBfecha(DireccionBuzon + Carta1))){
                                                           String ver = Util.leerTexto("Quieres ver la Carta(si)(no)").toLowerCase();
                                                           if (ver.equals("si")) {
                                                               Util.mostrar(logCarta.LeerCartas(DireccionBuzon + Carta1));
                                                               String aprobarCarta = Util.leerTexto("Quieres aprobar la Carta(si)(no)").toLowerCase();
                                                               if(aprobarCarta.equals("si")){
                                                                   logCarta.MoverA(DireccionBuzon + Carta1,Carta1,DireccionPendiente);
                                                               }if (!aprobarCarta.equals("si")) {
                                                                   logCarta.BorrarA(DireccionBuzon + Carta1);
                                                               }
                                                           }
                                                           else{
                                                           Util.mostrar("Siguiente");
                                                           }
                                                       }else{
                                                           Util.mostrarAlerta("Esta Carta no coinciden con la fecha que dijitó");
                                                       }
                                                    }else{
                                                        Util.mostrarAlerta("No tiene Cartas Disponibles");
                                                    }
                                                
                                                }
                                               
                                                break;
                                            case 2:
                                                String fecha = Util.leerTexto("Ingrese una Fecha:");
                                                
                                                for (String Carta1 : Cartas) {
                                                    
                                                    if (Carta1 != null) {
                                                       if (fecha.equals(logCarta.OBfecha(DireccionBuzon + Carta1))){
                                                           String ver = Util.leerTexto("Quieres ver la Carta(si)(no)").toLowerCase();
                                                           if (ver.equals("si")) {
                                                               Util.mostrar(logCarta.LeerCartas(DireccionBuzon + Carta1));
                                                               String aprobarCarta = Util.leerTexto("Quieres aprobar la Carta(si)(no)").toLowerCase();
                                                               if(aprobarCarta.equals("si")){
                                                                   logCarta.MoverA(DireccionBuzon + Carta1,Carta1,DireccionPendiente);
                                                               }if (!aprobarCarta.equals("si")) {
                                                                   logCarta.BorrarA(DireccionBuzon + Carta1);
                                                               }
                                                           }
                                                           else{
                                                           Util.mostrar("Siguiente");
                                                           }
                                                       }else{
                                                           Util.mostrarAlerta("Esta Carta no coinciden con la fecha que dijitó");
                                                       }
                                                    }else{
                                                        Util.mostrarAlerta("No tiene Cartas Disponibles");
                                                    }
                                                
                                                }
                                                break;
                                            case 3:
                                                break MENU5;
                                            default:
                                                Util.mostrar("Opcion Incorrecta");
                                        }
                                      }
                                    
                                case 2:
                                    MENU:
                                    while (true) {                                        
                                        int op6 = Util.leerInt(menuPedidos);
                                        switch (op6) {
                                            case 1:
                                                if (ManejoPedidos.ValidarPedidos(pedido)== true) {
                                                    Util.mostrar("\nLos pedidos que se han hecho hasta el momento son: \n");
                                                    ManejoPedidos.VerPedidos(pedido, juguete);
                                                    break;
                                                    
                                                }else{
                                                    Util.mostrarAlerta("\nNo tiene pedidos disponibles\n");
                                                }
                                                break;
                                                
                                            case 2:
//                                                Preguntar
                                                if (logUser.valores(logUser.leerNino())) {
                                                    String[] CartasPen = ArchivoPendientes.list();
                                                    if (logCarta.ValoresEnArreglo(CartasPen)) {
                                                        for (String Carta : CartasPen) {
                                                            if (Carta != null) {
                                                                 String cedulaNino;
                                                                 boolean validar;
                                                                String ver = Util.leerTexto("Quieres ver la Carta(si)(no)").toLowerCase();
                                                                if ("si".equals(ver)) {
                                                                    Util.mostrar(logCarta.LeerCartas(DireccionPendiente + Carta));
                                                                    String realizarPedido = Util.leerTexto("Quieres realizar el pedido(si)(no)").toLowerCase();
                                                                    if ("si".equals(realizarPedido)) {
                                                                        do {                                                                            
                                                                            cedulaNino = Util.leerTexto("Dijite la cedula del niño al que va dirigido el pedido o X para salir").toLowerCase();
                                                                            validar = logUser.verificarCedula_nino(cedulaNino,pedido);
                                                                            if(!validar){
                                                                                Util.mostrarAlerta("Cedula del niño no existe o tiene algun pedido asignado");
                                                                            }
                                                                        } while (!validar);
                                                                            if("x".equals(cedulaNino)){
                                                                                break;
                                                                            }
                                                                            String opJuguetes;
                                                                            do {
                                                                                opJuguetes = Util.leerTexto(menuJuguetes);
                                                                                validar =ManejoPedidos.PedidosJuguetes(cedulaNino, opJuguetes, juguete, pedido);
                                                                            if (!validar) {
                                                                                Util.mostrarAlerta("El numero seleccionado no esta en las opciones o no tiene insumos suficientes");
                                                                        }
                                                                            } while (!validar);
                                                                            if (opJuguetes.equals("X")) {
                                                                                break;
                                                                        }
                                                                            ManejoPedidos.RegistrarPedido(pedido);
                                                                            ManejoPedidos.VerPedidos(pedido, juguete);
                                                                            logCarta.MoverA(DireccionPendiente + Carta, Carta, DireccionProduccion);
                                                                            break;
                                                                            
                                                                    }if (!"si".equals(realizarPedido)) {
                                                                        Util.mostrar("Siguiente");
                                                                    }
                                                                    
                                                                }if (!"si".equals(Carta)) {
                                                                    Util.mostrar("Siguiente");
                                                                }
                                                            }else{
                                                                Util.mostrarAlerta("No hay cartas disponibles");
                                                            }}
                                                        break;
                                                    }else{
                                                        Util.mostrarAlerta("No hay cartas disponibles");
                                                    }
                                                }else{
                                                    Util.mostrarAlerta("No hay niños registrados");
                                                }
                                               
                                            case 3:
                                                if (logUser.valores(logUser.leerNino())) {
                                                    String[] CartasPen = ArchivoPendientes.list();
                                                    if (logCarta.ValoresEnArreglo(CartasPen)) {
                                                        
                                                        for (String Carta : CartasPen) {
                                                            if (Carta != null) {
                                                                 String cedulaNino;
                                                                 boolean validar;
                                                                String carta = Util.leerTexto("Quieres ver la Carta(si)(no)").toLowerCase();
                                                                if ("si".equals(carta)) {
                                                                    Util.mostrar(logCarta.LeerCartas(DireccionPendiente + Carta));
                                                                    String cancelarPedido = Util.leerTexto("Quieres Cancelar el pedido(si)(no)").toLowerCase();
                                                                    if ("si".equals(cancelarPedido)) {
                                                                        do {                                                                            
                                                                            cedulaNino = Util.leerTexto("Dijite la cedula del niño al que va dirigido el pedido o X para salir").toLowerCase();
                                                                            validar = ManejoPedidos.CancelarPedidos(cedulaNino, pedido);
                                                                            if(!validar){
                                                                                Util.mostrarAlerta("Cedula del niño no existe o tiene algun pedido Procesado");
                                                                            }
                                                                        } while (!validar);
                                                                            if("x".equals(cedulaNino)){
                                                                                break;
                                                                            }
                                                                            logCarta.BorrarA(DireccionPendiente + Carta);
                                                        }if (!"si".equals(cancelarPedido)){
                                                             Util.mostrar("Siguiente");
                                                          }
                                                                }if (!"si".equals(Carta)) {
                                                                    Util.mostrar("Siguiente");
                                                                }
                                                            }else{
                                                                Util.mostrarAlerta("No hay cartas disponibles");
                                                            }}
                                                        break;
                                                    }else{
                                                        Util.mostrarAlerta("No hay cartas disponibles");
                                                    }
                                                }else{
                                                        Util.mostrarAlerta("No hay Niños disponibles");
                                                    }
                                                break;
                                            case 4:
                                                break MENU5;
                                            default:
                                                Util.mostrarAlerta("Opcion Incorrecta");
                                        }
                                    }
                                   
                                case 3:
                                    SALIR_CONSULTA:
                                    while (true) {                                        
                                        int opConsulta = Util.leerInt(menuConsultas);
                                        switch (opConsulta) {
                                            case 1:
                                                PRODU_INSU:
                                                while (true) {                                                    
                                                    int op_Produ_Insumo = Util.leerInt(menuProdu_Insumo);
                                                    switch (op_Produ_Insumo) {
                                                        case 1:
                                                               Util.mostrar("Gastos de insumos diarios");
                                                               ManejoConsutal.PromedioXDia(historico);
                                                               
                                                               Util.mostrar("\nTotal de insumos que quedan en la fabrica");
                                                               ManejoConsutal.ImprimirTotalInsumos();
                                                               Util.mostrar("\nInsumosSuficientes");
                                                               ManejoConsutal.InsumosSuficientes(historico);
                                                            break;
                                                        case 2:
                                                            int Metal = Util.leerInt("Dijite la cantidad de Metal");
                                                            int Pintura = Util.leerInt("Dijite la cantidad de Pintura ");
                                                            int clavos = Util.leerInt("Dijite la cantidad de clavos ");
                                                            ManejoConsutal.PedirMasInsumos(Metal, Pintura, clavos);
                                                            ManejoConsutal.ImprimirTotalInsumos();
                                                            break;
                                                        case 3:
                                                            break  PRODU_INSU;
                                                        default:
                                                            Util.mostrarAlerta("Opcion Incorrecta");
                                                    }
                                                }
                                             
                                            case 2:
                                                Pedidos pedidos = new Pedidos();
                                                      if (ManejoPedidos.validarFinalizados(pedidos) == true){
                                                            Util.mostrar("\nPedidos finalizados:\n");
                                                            ManejoPedidos.ImprimirColillas(pedidos);
                                                            break;


                                                      }else{
                                                            Util.mostrar("\nNo tiene pedidos finalizados\n");
                                                            break;
                                                       }
                                                
                                            case 3:
                                                break MENU5;
                                            default:
                                                Util.mostrarAlerta("Opcion Incorrecta");
                                        }
                                    }
                                    
                                case 4:
                                    break MENU4;
                                    
                                default:
                                    Util.mostrarAlerta("Opcion Incorrecta");
                            }
                        }
                     //else   
                    }else{
                        if (!tipoUsuario) {
                             Util.mostrar("Usted se ha registrado como  Empleados ");
                             
                             MENU_EMPLEADO:
                             while (true) {                                
                                int opEmpleado = Util.leerInt(menuEmpleado);
                                 switch (opEmpleado) {
                                     case 1:
                                         String cedulaID = Util.leerTexto("Digite su cedula:");
                                         String nombre = Util.leerTexto("Digite su nombre:");
                                         String edad = Util.leerTexto("Ingrese su Edad");
                                         String direccion = Util.leerTexto("Digite su direccion:");
                                         Ninos temp = new Ninos ( cedulaID, nombre, edad,direccion);
                                         logUser.guardarNino(temp);
                                         Util.mostrar("El niño se Agregó con Exito");
                                         break;
                                     case 2:
                                         if (logUser.valores(logUser.leerNino())) {
                                                    String[] CartasPen = ArchivoPendientes.list();
                                                    if (logCarta.ValoresEnArreglo(CartasPen)) {
                                                        for (String Carta : CartasPen) {
                                                            if (Carta != null) {
                                                                 String cedulaNino;
                                                                 boolean validar;
                                                                String ver = Util.leerTexto("Quieres ver la Carta(si)(no)").toLowerCase();
                                                                if ("si".equals(ver)) {
                                                                    Util.mostrar(logCarta.LeerCartas(DireccionPendiente + Carta));
                                                                    String realizarPedido = Util.leerTexto("Quieres realizar el pedido(si)(no)").toLowerCase();
                                                                    if ("si".equals(realizarPedido)) {
                                                                        do {                                                                            
                                                                            cedulaNino = Util.leerTexto("Dijite la cedula del niño al que va dirigido el pedido o X para salir").toLowerCase();
                                                                            validar = logUser.verificarCedula_nino(cedulaNino, pedido);
                                                                            if(!validar){
                                                                                Util.mostrarAlerta("Cedula del niño no existe o tiene algun pedido asignado");
                                                                            }
                                                                        } while (!validar);
                                                                            if("x".equals(cedulaNino)){
                                                                                break;
                                                                            }
                                                                            String opJuguetes;
                                                                            do {
                                                                                opJuguetes = Util.leerTexto(menuJuguetes);
                                                                                validar =ManejoPedidos.PedidosJuguetes(cedulaNino, opJuguetes, juguete, pedido);
                                                                            if (!validar) {
                                                                                Util.mostrarAlerta("El numero seleccionado no esta en las opciones o no tiene insumos suficientes");
                                                                        }
                                                                            } while (!validar);
                                                                            if (opJuguetes.equals("X")) {
                                                                                break;
                                                                        }
                                                                            ManejoPedidos.RegistrarPedido(pedido);
                                                                            ManejoPedidos.VerPedidos(pedido, juguete);
                                                                            logCarta.MoverA(DireccionPendiente + Carta, Carta, DireccionProduccion); 
                                                                            break;
                                                                            
                                                                    }if (!"si".equals(realizarPedido)) {
                                                                        Util.mostrar("Siguiente");
                                                                    }
                                                                    
                                                                }if (!"si".equals(Carta)) {
                                                                    Util.mostrar("Siguiente");
                                                                }
                                                            }else{
                                                                Util.mostrarAlerta("No hay cartas disponibles");
                                                            }}
                                                        break;
                                                    }else{
                                                        Util.mostrarAlerta("No hay cartas disponibles");
                                                    }
                                                }else{
                                                    Util.mostrarAlerta("No hay niños registrados");
                                                }
                                         break;
                                     case 3:
                                         boolean validar;
                                         String iD_Nino;
                                         if(ManejoPedidos.ValidarPedidosFinalizados(pedido)==true){
                                            Util.mostrar("\nLos pedidos que se han hecho hasta el momento son: \n");
                                            ManejoPedidos.VerPedidos(pedido, juguete);
                                            do {
                                                 iD_Nino = Util.leerTexto("Dijite la cedula del niño al que va a finalizarle el pedido o X para salir");
                                                 validar = ManejoPedidos.IDNinosFinalizar(pedido,iD_Nino);
                                                if(!validar){
                                                  Util.mostrarAlerta("La cedula del niño que dijito no existe o ya el pedido del niño esta finalizado");
                                                       }
                                                 }while (!validar);
                                             if ("X".equals(iD_Nino)){
                                                 
                                                  break;
                                                  
                                             }
                                            
                                            ManejoPedidos.RecetasDeJuguetes(iD_Nino, pedido, juguete);
                                            ManejoPedidos.VerPedidos(pedido,juguete);
                                            
                                            break;
                                         }
                                         else{
                                             Util.mostrarAlerta("No tiene pedidos");
                                         }
                                         break;
                                     case 4:
                                         if (ManejoPedidos.validarFinalizados(pedido) == true){
                                             Util.mostrar("\nPedidos finalizados:\n");
                                             ManejoPedidos.ImprimirColillas(pedido);
                                             break;
                                          
                                 
                                       }else{
                                             System.out.println("\nNo tiene pedidos finalizados\n");
                                        }
                                         break;
                                     case 5:
                                         break APP;
                                     default:
                                         Util.mostrarAlerta("Opcion Incorrecta");
                                 }
                            }
                        }
                    }
                    
                    
                default:
                    Util.mostrarAlerta("Opcion Incorrecta");
                    
            }
        }
    }
    
}
