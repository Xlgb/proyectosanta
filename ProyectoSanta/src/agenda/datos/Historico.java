/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda.datos;

/**
 *
 * @author dell
 */
public class Historico {
    
    private String PorDia;
    private int Madera;
    private int Pintura;
    private int Clavos;

    public Historico() {
    }

    public Historico(String PorDia, int Madera, int Pintura, int Calvos) {
        this.PorDia = PorDia;
        this.Madera = Madera;
        this.Pintura = Pintura;
        this.Clavos = Calvos;
    }

    public String getPorDia() {
        return PorDia;
    }

    public void setPorDia(String PorDia) {
        this.PorDia = PorDia;
    }

    public int getMadera() {
        return Madera;
    }

    public void setMadera(int Madera) {
        this.Madera = Madera;
    }

    public int getPintura() {
        return Pintura;
    }

    public void setPintura(int Pintura) {
        this.Pintura = Pintura;
    }

    public int getClavos() {
        return Clavos;
    }

    public void setCalvos(int Calvos) {
        this.Clavos = Calvos;
    }
    
    
}
