/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda.datos;

import java.text.SimpleDateFormat;

/**
 *
 * @author dell
 */
public class Carta {
    private String Fecha;
    private String Nombre;
    private String Destinatario;
    private String Cuerpo_Carta;
    private boolean Tipo_Carta;

    public Carta() {
    }

    public Carta(String Fecha, String Nombre, String Destinatario, String Cuerpo_Carta, boolean Tipo_Carta) {
        this.Fecha = Fecha;
        this.Nombre = Nombre;
        this.Destinatario = Destinatario;
        this.Cuerpo_Carta = Cuerpo_Carta;
        this.Tipo_Carta = Tipo_Carta;
    }
    

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String Fecha) {
        this.Fecha = Fecha;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getDestinatario() {
        return Destinatario;
    }

    public void setDestinatario(String Destinatario) {
        this.Destinatario = Destinatario;
    }

    public String getCuerpo_Carta() {
        return Cuerpo_Carta;
    }

    public void setCuerpo_Carta(String Cuerpo_Carta) {
        this.Cuerpo_Carta = Cuerpo_Carta;
    }

    public boolean isTipo_Carta() {
        return Tipo_Carta;
    }

    public void setTipo_Carta(boolean Tipo_Carta) {
        this.Tipo_Carta = Tipo_Carta;
    }
    public String getInfo() {
        String temp = "%s,%s,%s,%s,%d";
        return String.format(temp,  Fecha, Nombre,  Destinatario,  Cuerpo_Carta,  Tipo_Carta);
    }

    @Override
    public String toString() {
        return "Carta{" + "Fecha=" + Fecha + ", Nombre=" + Nombre + ", Destinatario=" + Destinatario + ", Cuerpo_Carta=" + Cuerpo_Carta + ", Tipo_Carta=" + Tipo_Carta + '}';
    }
    
    

    
    
    
    
}
