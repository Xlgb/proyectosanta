/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda.datos;

/**
 *
 * @author dell
 */
public class Juguete {
    
    private String Codigo;
    private String Nombre;
    private int Madera;
    private int Pintura;
    private int Clavos;

    public Juguete() {
    }

    public Juguete(String Codigo, String Nombre, int Madera, int Pintura, int Clavos) {
        this.Codigo = Codigo;
        this.Nombre = Nombre;
        this.Madera = Madera;
        this.Pintura = Pintura;
        this.Clavos = Clavos;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public int getMadera() {
        return Madera;
    }

    public void setMadera(int Madera) {
        this.Madera = Madera;
    }

    public int getPintura() {
        return Pintura;
    }

    public void setPintura(int Pintura) {
        this.Pintura = Pintura;
    }

    public int getClavos() {
        return Clavos;
    }

    public void setClavos(int Clavos) {
        this.Clavos = Clavos;
    }
    
    
}
