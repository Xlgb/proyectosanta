/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda.datos;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author dell
 */
public class Usuario {
    private String CedulaID;
    private String Nombre;
    private String Contrasena;
    private Date FechaNacimiento;
    private Boolean TipoUsuario;
    private String Correo;

    public Usuario() {
    }

    public Usuario(String CedulaID, String Nombre, String Contrasena, Date FechaNacimiento, Boolean TipoUsuario, String Correo) {
        this.CedulaID = CedulaID;
        this.Nombre = Nombre;
        this.Contrasena = Contrasena;
        this.FechaNacimiento = FechaNacimiento;
        this.TipoUsuario = TipoUsuario;
        this.Correo = Correo;
    }

    public String getCedulaID() {
        return CedulaID;
    }

    public void setCedulaID(String CedulaID) {
        this.CedulaID = CedulaID;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getContrasena() {
        return Contrasena;
    }

    public void setContrasena(String Contrasena) {
        this.Contrasena = Contrasena;
    }

    public Date getFechaNacimiento() {
        return FechaNacimiento;
    }
    public String getFechaNacimientoString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(FechaNacimiento);
    }

    public void setFechaNacimiento(Date FechaNacimiento) {
        this.FechaNacimiento = FechaNacimiento;
    }

    public Boolean getTipoUsuario() {
        return TipoUsuario;
    }

    public void setTipoUsuario(Boolean TipoUsuario) {
        this.TipoUsuario = TipoUsuario;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String Correo) {
        this.Correo = Correo;
    }
    public String getInfo() {
        String temp = "%s,%s,%s,%s,%s,%s";
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return String.format(temp,  CedulaID, Nombre,  Contrasena,  getFechaNacimientoString(),  TipoUsuario,  Correo);
    }

    @Override
    public String toString() {
        return "Usuario{" + "CedulaID=" + CedulaID + ", Nombre=" + Nombre + ", Contrasena=" + Contrasena + ", FechaNacimiento=" + FechaNacimiento + ", TipoUsuario=" + TipoUsuario + ", Correo=" + Correo + '}';
    }

    
    

     
}
