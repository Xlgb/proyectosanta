/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda.datos;

/**
 *
 * @author dell
 */
public class Pedidos {

    private String Nombre;
    private String ID;
    private String Regalo;
    private boolean Finalizado;
    
    
    public Pedidos(){
        
        
    }
    public Pedidos(String Nombre, String ID, String Regalo, boolean Finalizado) {
        this.Nombre = Nombre;
        this.ID = ID;
        this.Regalo = Regalo;
        this.Finalizado = Finalizado;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getRegalo() {
        return Regalo;
    }

    public void setRegalo(String Regalo) {
        this.Regalo = Regalo;
    }

    public boolean isFinalizado() {
        return Finalizado;
    }

    public void setFinalizado(boolean Finalizado) {
        this.Finalizado = Finalizado;
    }
    
}

