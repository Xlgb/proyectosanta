/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda.datos;

import java.text.SimpleDateFormat;

/**
 *
 * @author dell
 */
public class Ninos {
    private String CedulaID;
    private String Nombre;
    private String Edad;
    private String direccion;

    public Ninos() {
    }

    public Ninos(String CedulaID, String Nombre, String Edad, String direccion) {
        this.CedulaID = CedulaID;
        this.Nombre = Nombre;
        this.Edad = Edad;
        this.direccion = direccion;
    }

    public String getCedulaID() {
        return CedulaID;
    }

    public void setCedulaID(String CedulaID) {
        this.CedulaID = CedulaID;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getEdad() {
        return Edad;
    }

    public void setEdad(String Edad) {
        this.Edad = Edad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
     public String getInfo() {
        String temp = "%s,%s,%s,%s";
        return String.format(temp,  CedulaID, Nombre,  Edad,  direccion);
    }

    @Override
    public String toString() {
        return String.format("%s,%s,%s,%s",CedulaID,Nombre,Edad,direccion);
    }

    public String[] split(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    

    
}
